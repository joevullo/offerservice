package com.joevullo.offerservice;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Currency;

public class OfferTest {

    @Test
    public void whenOfferStatusIsSetToNull_ThenOfferStatusIsCalculated() {

        LocalDate baseDate = LocalDateTime.now().toLocalDate();

        Offer offer = OfferBuilder.create()
                .withPrice(50.52)
                .withStartDate(baseDate.minusWeeks(4))
                .withEndDate(baseDate.plusWeeks(4))
                .withDescription("This is a description")
                .withCurrency(Currency.getInstance("GBP"))
                .withOfferStatus(null)
                .build();

        assertThat(offer.getOfferStatus(), is(OfferStatus.ACTIVE));
    }
}