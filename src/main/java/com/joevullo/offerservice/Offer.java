package com.joevullo.offerservice;


import java.time.LocalDate;
import java.util.Currency;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <p>
 * "an offer is a proposal to sell a specific product or service under specific conditions".
 *
 * An offer is time-bounded, with the length of time an offer is valid for defined as part of the offer, a
 * nd should expire automatically. Offers may also be explicitly cancelled before they expire.
 *
 * All my offers have shopper friendly descriptions. I price all my offers up front in a defined currency.
 *
 * Class currently uses dates instead of datetimes for simplicity.
 *</p>
 */
@Entity
@Table
public class Offer {

    @Id
    @GeneratedValue
    private long id;

    private LocalDate startDate;

    private LocalDate endDate;

    private String description;

    private OfferStatus offerStatus;

    private Currency currency;

    private Number price;

    public Offer() {}

    public Offer(LocalDate startDate, LocalDate endDate, String description, Currency currency, Number price, OfferStatus offerStatus) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.description = description;
        this.currency = currency;
        this.price = price;

        if (offerStatus == null) {
            this.offerStatus = OfferStatusCalculator.calculateOfferStatus(startDate, endDate);
        } else {
            this.offerStatus = offerStatus;
        }
    }

    public long getId() {
        return id;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public String getDescription() {
        return description;
    }

    public OfferStatus getOfferStatus() {
        return offerStatus;
    }

    public Currency getCurrency() {
        return currency;
    }

    public Number getPrice() {
        return price;
    }

    public void setOfferStatus(OfferStatus offerStatus) {
        this.offerStatus = offerStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Offer offer = (Offer) o;
        return id == offer.id &&
                Objects.equals(startDate, offer.startDate) &&
                Objects.equals(endDate, offer.endDate) &&
                Objects.equals(description, offer.description) &&
                offerStatus == offer.offerStatus &&
                Objects.equals(currency, offer.currency) &&
                Objects.equals(price, offer.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, startDate, endDate, description, offerStatus, currency, price);
    }

    @Override
    public String toString() {
        return "Offer{" +
                "id=" + id +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", description='" + description + '\'' +
                ", offerStatus=" + offerStatus +
                ", currency=" + currency +
                ", price=" + price +
                '}';
    }
}
