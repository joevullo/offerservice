package com.joevullo.offerservice;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@RunWith(SpringRunner.class)
@EnableWebMvc
@WebMvcTest(TestController.class)
public class ApiExceptionHandlerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void whenControllerReturnsAsExpected_ThenHttpOkStatusIsReturned() throws Exception {
        mockMvc.perform(get("/testcontroller/testOk"))
                .andExpect(status().isOk());
    }

    @Test
    public void whenDataAccessExceptionIsThrownByController_ThenApiExceptionHandlerReturnsHttpInternalServerError() throws Exception {
        mockMvc.perform(get("/testcontroller/testOfferNotFoundException"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void whenDataAccessExceptionIsThrownByController_ThenApiExceptionHandlerReturnsAHttpBadRequest() throws Exception {
        mockMvc.perform(get("/testcontroller/testIllegalArgumentException"))
                .andExpect(status().isBadRequest());
    }
}