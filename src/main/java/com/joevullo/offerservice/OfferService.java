package com.joevullo.offerservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OfferService {

    private OfferRepository repository;

    @Autowired
    public OfferService(OfferRepository repository) {
        this.repository = repository;
    }

    private static boolean activeOffer(Offer o) {
        return o.getOfferStatus() == OfferStatus.ACTIVE;
    }

    private static void updateOfferStatus(Offer o) {
        o.setOfferStatus(OfferStatusCalculator.calculateOfferStatus(o));
    }

    public Offer persistNewOffer(Offer offer) {
        return repository.save(offer);
    }

    public Offer getOffer(Long id) throws OfferNotFoundException {
        Offer offer = Optional.ofNullable(repository.findOne(id))
                .orElseThrow(() -> new OfferNotFoundException(id));

        if (activeOffer(offer)) {
            updateOfferStatus(offer);
        }

        return offer;
    }

    public List<Offer> getAllOffers() {

        List<Offer> offerList = repository.findAll();

        offerList.stream()
                .filter(OfferService::activeOffer)
                .forEach(OfferService::updateOfferStatus);

        return offerList;
    }

    public Offer replaceOfferStatus(Long id, OfferStatus offerStatus) throws OfferNotFoundException {
        Offer offer = repository.findOne(id);

        if (offer != null) {
            offer.setOfferStatus(offerStatus);
            return repository.save(offer);
        } else {
            throw new OfferNotFoundException(id);
        }
    }
}
