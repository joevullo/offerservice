package com.joevullo.offerservice;

import static com.joevullo.offerservice.OfferObjectMother.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class OfferServiceTest {

    @Mock
    private OfferRepository offerRepository;

    @InjectMocks
    private OfferService offerService;

    @Test(expected = OfferNotFoundException.class)
    public void givenFindOneReturnsNull_WhenOfferServiceGetOne_ThenOfferNotFoundExceptionIsThrow() throws OfferNotFoundException {
        when(offerRepository.findOne(1L)).thenReturn(null);
        offerService.getOffer(1L);
    }

    @Test(expected = OfferNotFoundException.class)
    public void givenFindOneReturnsNull_WhenOfferServiceReplaceOfferStatus_ThenOfferNotFoundExceptionIsThrow() throws OfferNotFoundException {
        when(offerRepository.findOne(1L)).thenReturn(null);
        offerService.replaceOfferStatus(1L, OfferStatus.CANCELLED);
    }

    @Test
    public void givenFindAllReturnsEmptyList_WhenOfferServiceReplaceOfferStatus_ThenEmptyListIsReturned() {
        when(offerRepository.findAll()).thenReturn(Collections.emptyList());
        List<Offer> allOffers = offerService.getAllOffers();
        assertThat(allOffers.isEmpty(), is(true));
    }

    @Test
    public void givenADefaultOfferIsReturnedFromRepository_WhenReplaceOfferStatusWithOfferStatusCancelled_ThenAnUpdatedOfferIsSavedWithOfferStatusOfCancelled() throws OfferNotFoundException {
        when(offerRepository.findOne(1L)).thenReturn(aDefaultOffer());
        offerService.replaceOfferStatus(1L, OfferStatus.CANCELLED);

        Offer offer = aDefaultOffer();
        offer.setOfferStatus(OfferStatus.CANCELLED);
        verify(offerRepository).save(offer);
    }

    @Test
    public void givenRepositoryFindOneReturnsADefaultOffer_WhenOfferServiceGetOffer_ThenOfferReturnedIsCorrect() throws OfferNotFoundException {
        when(offerRepository.findOne(1L)).thenReturn(aDefaultOffer());
        Offer offer = offerService.getOffer(1L);
        assertThat(offer, is(aDefaultOffer()));
    }

    @Test
    public void givenRepositoryFindAllReturnsADefaultOffer_WhenOfferServiceGetAllOffer_ThenOfferListReturnedIsCorrect() {
        when(offerRepository.findAll()).thenReturn(aListOfDefaultOffer());
        List<Offer> allOffers = offerService.getAllOffers();
        assertThat(allOffers, is(aListOfDefaultOffer()));
    }

    @Test
    public void givenRepositoryFindOneReturnsAnOfferWithActiveStateWhoShouldBeExpired_ThenOfferServiceReturnsAnOfferWithExpired() throws OfferNotFoundException {
        when(offerRepository.findOne(1L)).thenReturn(anOfferWithActiveStateButDatesMeanExpired());
        assertThat(offerService.getOffer(1L).getOfferStatus(), is(OfferStatus.EXPIRED));
    }

    @Test
    public void givenRepositoryFindAllReturnsAnOfferWithActiveStateWhoShouldBeExpired_ThenOfferServiceReturnsAnOfferWithExpired() throws OfferNotFoundException {
        when(offerRepository.findAll()).thenReturn(Collections.singletonList(anOfferWithActiveStateButDatesMeanExpired()));

        assertThat(offerService.getAllOffers().size(), is(1));
        assertThat(offerService.getAllOffers().get(0).getOfferStatus(), is(OfferStatus.EXPIRED));
    }

    @Test
    public void givenRepositoryFindAllReturnsAnOfferWithCancelledStateWhosDatesMeanTheyCouldBeExpired_ThenOfferServiceReturnsAnOfferWithCancelled() throws OfferNotFoundException {
        when(offerRepository.findAll()).thenReturn(Collections.singletonList(anOfferWithActiveStateButDatesMeanExpired()));

        assertThat(offerService.getAllOffers().size(), is(1));
        assertThat(offerService.getAllOffers().get(0).getOfferStatus(), is(OfferStatus.EXPIRED));
    }
}