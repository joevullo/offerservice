package com.joevullo.offerservice;

public enum OfferStatus {
    ACTIVE,
    CANCELLED,
    EXPIRED
}
