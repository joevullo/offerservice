package com.joevullo.offerservice;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class OfferStatusCalculator {

    public static OfferStatus calculateOfferStatus(LocalDate startDate, LocalDate endDate, LocalDateTime currentTime) {

        if (startDate == null) {
            throw new IllegalArgumentException("startDate cannot be null");
        }

        if (endDate == null) {
            throw new IllegalArgumentException("startDate cannot be null");
        }

        if (currentTime == null) {
            throw new IllegalArgumentException("startDate cannot be null");
        }

        if (startDate.isAfter(endDate)) {
            throw new IllegalArgumentException(("Start date should be before endDate." +
                    " startDate = " + startDate +
                    " endDate = " + endDate));
        }

        LocalDate currentDate = currentTime.toLocalDate();
        if ((currentDate.isAfter((startDate)) && currentDate.isBefore(endDate))
                || currentDate.equals(startDate)
                || currentDate.equals(endDate)) {
            return OfferStatus.ACTIVE;
        } else {
            return OfferStatus.EXPIRED;
        }
    }


    public static OfferStatus calculateOfferStatus(Offer offer) {
        return calculateOfferStatus(offer.getStartDate(), offer.getEndDate());
    }

    public static OfferStatus calculateOfferStatus(LocalDate startDate, LocalDate endDate) {
        return calculateOfferStatus(startDate, endDate, LocalDateTime.now());
    }

}
