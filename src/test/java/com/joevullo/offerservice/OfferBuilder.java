package com.joevullo.offerservice;

import java.time.LocalDate;
import java.util.Currency;

public class OfferBuilder {

    private LocalDate startDate;
    private LocalDate endDate;
    private String description;
    private OfferStatus offerStatus;
    private Currency currency;
    private Number price;

    private OfferBuilder() { }

    public static OfferBuilder create() {
        return new OfferBuilder();
    }

    public OfferBuilder withStartDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    public OfferBuilder withEndDate(LocalDate endDate) {
        this.endDate = endDate;
        return this;
    }

    public OfferBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public OfferBuilder withOfferStatus(OfferStatus offerStatus) {
        this.offerStatus = offerStatus;
        return this;
    }

    public OfferBuilder withCurrency(Currency currency) {
        this.currency = currency;
        return this;
    }

    public OfferBuilder withPrice(Number price) {
        this.price = price;
        return this;
    }

    public Offer build() {
        return new Offer(startDate, endDate, description, currency, price, offerStatus);
    }

}
