package com.joevullo.offerservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * From the SPEC:
 * Offers, once created, may be queried. After the period of time defined on the offer it should expire and
 * further requests to query the offer should reflect that somehow. Before an offer has expired users may cancel it.
 *
 */
@RestController
@RequestMapping(value = {"/v1.0/offer"})
public class OfferController {

    private OfferService offerService;

    @Autowired
    public OfferController(OfferService offerService) {
        this.offerService = offerService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    private Offer persistNewOffer(@RequestBody Offer offer) {
        return offerService.persistNewOffer(offer);
    }

    @GetMapping("{id}")
    private Offer getOffer(@PathVariable Long id) throws OfferNotFoundException {
        return offerService.getOffer(id);
    }

    @GetMapping
    private List<Offer> getAllOffers() {
        return offerService.getAllOffers();
    }


    @PutMapping("{id}/offer_status")
    private Offer replaceOfferStatus(@PathVariable Long id, @RequestBody OfferStatus offerStatus) throws OfferNotFoundException {
        return offerService.replaceOfferStatus(id, offerStatus);
    }
}
