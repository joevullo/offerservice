package com.joevullo.offerservice;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;

public class OfferObjectMother {

    public static List<Offer> aListOfDefaultOffer() {
        return Arrays.asList(
                aDefaultOffer(),
                aDefaultOffer(),
                aDefaultOffer(),
                aDefaultOffer(),
                aDefaultOffer()
        );
    }

    public static Offer aDefaultOffer() {
        return aDefaultOfferBuilder()
                .build();
    }

    public static Offer anOfferWithActiveStateButDatesMeanExpired() {
        return aDefaultOfferBuilder()
                .withEndDate(LocalDate.now().minusYears(10))
                .withStartDate(LocalDate.now().minusYears(10))
                .withOfferStatus(OfferStatus.ACTIVE)
                .build();
    }

    private static OfferBuilder aDefaultOfferBuilder() {
        return OfferBuilder.create()
                .withCurrency(Currency.getInstance("GBP"))
                .withDescription("This is an offer")
                .withEndDate(LocalDate.of(2022,1,1))
                .withStartDate(LocalDate.of(2019,1,1))
                .withPrice(50.41)
                .withOfferStatus(OfferStatus.ACTIVE);
    }
}
