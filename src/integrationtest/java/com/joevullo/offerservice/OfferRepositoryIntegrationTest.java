package com.joevullo.offerservice;

import static com.joevullo.offerservice.OfferObjectMother.aDefaultOffer;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * A very basic test to see if my datasource / setup is working as expected.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class OfferRepositoryIntegrationTest {

    @Autowired
    private OfferRepository offerRepository;

    @Test
    public void givenAnOfferIsPersistedToDbAndRetrieved_ThenOfferIsCorrect() {
        Offer offer = aDefaultOffer();
        offerRepository.save(offer);
        Offer one = offerRepository.findOne(1L);
        assertThat(one, is(offer));

    }
}