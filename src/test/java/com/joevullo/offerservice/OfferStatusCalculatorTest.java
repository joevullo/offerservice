package com.joevullo.offerservice;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class OfferStatusCalculatorTest {

    private static LocalDate DEFAULT_OFFER_START_DATE = LocalDate.of(2018,1,1);
    private static LocalDate DEFAULT_OFFER_END_DATE = LocalDate.of(2018,3,1);
    private static LocalDateTime DEFAULT_CURRENT_TIME = LocalDateTime.of(2018,2,1,12,12,12);

    @Test(expected = IllegalArgumentException.class)
    public void whenStartDateIsNull_ThenIllegalArgumentExceptionIsThrown() {
        OfferStatusCalculator.calculateOfferStatus(
                null,
                DEFAULT_OFFER_END_DATE,
                DEFAULT_CURRENT_TIME);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenEndDateIsNull_ThenIllegalArgumentExceptionIsThrown() {
        OfferStatusCalculator.calculateOfferStatus(
                DEFAULT_OFFER_START_DATE,
                null,
                DEFAULT_CURRENT_TIME);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenCurrentTimeIsNull_ThenIllegalArgumentExceptionIsThrown() {
        OfferStatusCalculator.calculateOfferStatus(
                DEFAULT_OFFER_START_DATE,
                DEFAULT_OFFER_END_DATE,
                null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenAllValuesAreNull_ThenIllegalArgumentExceptionIsThrown() {
        OfferStatusCalculator.calculateOfferStatus(
                null,
                null,
                null);

    }

    @Test(expected = IllegalArgumentException.class)
    public void whenStartDateIsAfterEndDate_ThenIllegalArgumentExceptionIsThrown() {

        OfferStatus result = OfferStatusCalculator.calculateOfferStatus(
                DEFAULT_OFFER_END_DATE,
                DEFAULT_OFFER_START_DATE,
                DEFAULT_CURRENT_TIME);

        assertThat(result, is(OfferStatus.EXPIRED));
    }

    @Test
    public void whenCurrentTimeIsBetweenOfferStartAndEndDates_ThenResultIsOfferStatusOfActive() {

        OfferStatus result = OfferStatusCalculator.calculateOfferStatus(
                DEFAULT_OFFER_START_DATE,
                DEFAULT_OFFER_END_DATE,
                DEFAULT_CURRENT_TIME);

        assertThat(result, is(OfferStatus.ACTIVE));
    }

    @Test
    public void whenCurrentTimeIsAfterOfferStartAndEndDates_ThenResultIsOfferStatusOfExpired() {

        LocalDateTime currentTime = LocalDateTime.of(2019,1,1,0,0,0);

        OfferStatus result = OfferStatusCalculator.calculateOfferStatus(
                DEFAULT_OFFER_START_DATE,
                DEFAULT_OFFER_END_DATE,
                currentTime);

        assertThat(result, is(OfferStatus.EXPIRED));
    }

    @Test
    public void whenStartAndEndTimeAreTheSameDay_ThenResultIsOfferStatusOfExpired() {

        OfferStatus result = OfferStatusCalculator.calculateOfferStatus(
                DEFAULT_OFFER_START_DATE,
                DEFAULT_OFFER_START_DATE,
                LocalDateTime.of(DEFAULT_OFFER_START_DATE, LocalTime.of(12,10)));

        assertThat(result, is(OfferStatus.ACTIVE));
    }

    @Test
    public void whenUsingCalculateOfferStatusWithNoCurrentTimeInput_ThenResultIsCorrect() {
        OfferStatus result = OfferStatusCalculator.calculateOfferStatus(
                LocalDate.now().minusWeeks(1),
                LocalDate.now().plusWeeks(1));

        assertThat(result, is(OfferStatus.ACTIVE));
    }

    @Test
    public void whenCurrentTimeIsBetweenSameDateWithMinTimeAsStartDate_ThenResultIsOfferStatusOfActive() {

        OfferStatus result = OfferStatusCalculator.calculateOfferStatus(
                DEFAULT_OFFER_START_DATE,
                DEFAULT_OFFER_END_DATE,
                LocalDateTime.of(DEFAULT_OFFER_START_DATE, LocalTime.MIN));

        assertThat(result, is(OfferStatus.ACTIVE));
    }

    @Test
    public void whenCurrentTimeIsBetweenSameDateWithMaxTimeAsStartDate_ThenResultIsOfferStatusOfActive() {

        OfferStatus result = OfferStatusCalculator.calculateOfferStatus(
                DEFAULT_OFFER_START_DATE,
                DEFAULT_OFFER_END_DATE,
                LocalDateTime.of(DEFAULT_OFFER_START_DATE, LocalTime.MAX));

        assertThat(result, is(OfferStatus.ACTIVE));
    }

    @Test
    public void whenCurrentTimeIsBetweenSameDateWithMinTimeAsEndDate_ThenResultIsOfferStatusOfActive() {

        OfferStatus result = OfferStatusCalculator.calculateOfferStatus(
                DEFAULT_OFFER_START_DATE,
                DEFAULT_OFFER_END_DATE,
                LocalDateTime.of(DEFAULT_OFFER_END_DATE, LocalTime.MIN));

        assertThat(result, is(OfferStatus.ACTIVE));
    }

    @Test
    public void whenCurrentTimeIsBetweenSameDateWithMaxTimeAsEndDate_ThenResultIsOfferStatusOfActive() {

        OfferStatus result = OfferStatusCalculator.calculateOfferStatus(
                DEFAULT_OFFER_START_DATE,
                DEFAULT_OFFER_END_DATE,
                LocalDateTime.of(DEFAULT_OFFER_END_DATE, LocalTime.MAX));

        assertThat(result, is(OfferStatus.ACTIVE));
    }
}