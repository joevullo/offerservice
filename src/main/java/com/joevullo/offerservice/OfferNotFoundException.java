package com.joevullo.offerservice;

public class OfferNotFoundException extends Exception {

    public OfferNotFoundException(Long id) {
        super("Offer with ID of " + id + " was not found");
    }

    public OfferNotFoundException(String message) {
        super(message);
    }
}
