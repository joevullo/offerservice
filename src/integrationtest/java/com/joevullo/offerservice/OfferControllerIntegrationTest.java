package com.joevullo.offerservice;

import static com.joevullo.offerservice.OfferObjectMother.aDefaultOffer;
import static com.joevullo.offerservice.OfferObjectMother.aListOfDefaultOffer;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

@RunWith(SpringRunner.class)
@WebMvcTest(OfferController.class)
public class OfferControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OfferService offerService;

    private String rootApi = "/v1.0/offer";

    @Test
    public void givenOfferServiceGetAllReturnsEmptyList_ThenGetOfferShowsEmptyContent() throws Exception {
        when(offerService.getAllOffers()).thenReturn(Collections.emptyList());

        mockMvc.perform(get(rootApi))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.length()", is(0)));
    }

    @Test
    public void whenPostingAnOffer_ThenOfferIsCreatedAndContentReturnedIsCorrect() throws Exception {
        when(offerService.persistNewOffer(anyObject())).thenAnswer(i -> i.getArguments()[0]);

        String inputJson =
                "{ " +
                "  \"start_date\" : \"2019-01-01\"," +
                "  \"end_date\" : \"2022-01-01\"," +
                "  \"description\" : \"This is an offer\"," +
                "  \"currency\" : \"GBP\"," +
                "  \"price\" : 50.41" +
                " }";

        mockMvc.perform(post(rootApi)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(inputJson))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id", is(0)))
                .andExpect(jsonPath("$.start_date", is("2019-01-01")))
                .andExpect(jsonPath("$.end_date", is("2022-01-01")))
                .andExpect(jsonPath("$.description", is("This is an offer")))
                .andExpect(jsonPath("$.currency", is("GBP")))
                .andExpect(jsonPath("$.price", is(50.41)));
    }

    @Test
    public void givenOfferServiceGetAllOffers_WhenGetOnOffer_ThenCorrectContentResponse() throws Exception {

        when(offerService.getAllOffers())
                .thenReturn(aListOfDefaultOffer());

        mockMvc.perform(get(rootApi))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.length()", is(5)))
                .andExpect(jsonPath("$.[0].id", is(0)))
                .andExpect(jsonPath("$.[0].start_date", is("2019-01-01")))
                .andExpect(jsonPath("$.[0].end_date", is("2022-01-01")))
                .andExpect(jsonPath("$.[0].description", is("This is an offer")))
                .andExpect(jsonPath("$.[0].offer_status", is("ACTIVE")))
                .andExpect(jsonPath("$.[0].currency", is("GBP")))
                .andExpect(jsonPath("$.[0].price", is(50.41)));
    }

    @Test
    public void givenOfferServiceGetOfferReturnsDefaultOffer_WhenGetOnSingleOffer_ThenCorrectContentResponse() throws Exception {

        when(offerService.getOffer(1L)).thenReturn(aDefaultOffer());

        mockMvc.perform(get(rootApi + "/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id", is(0)))
                .andExpect(jsonPath("$.start_date", is("2019-01-01")))
                .andExpect(jsonPath("$.end_date", is("2022-01-01")))
                .andExpect(jsonPath("$.description", is("This is an offer")))
                .andExpect(jsonPath("$.offer_status", is("ACTIVE")))
                .andExpect(jsonPath("$.currency", is("GBP")))
                .andExpect(jsonPath("$.price", is(50.41)));
    }

    @Test
    public void givenOfferServiceReplaceOfferStatusReturnsDefaultOffer_WhenPutOnOfferOfferStatus_ThenCorrectContentResponse() throws Exception {

        when(offerService.replaceOfferStatus(1L, OfferStatus.ACTIVE)).thenReturn(aDefaultOffer());

        mockMvc.perform(put(rootApi + "/1/" + "offer_status")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content("\"ACTIVE\""))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id", is(0)))
                .andExpect(jsonPath("$.start_date", is("2019-01-01")))
                .andExpect(jsonPath("$.end_date", is("2022-01-01")))
                .andExpect(jsonPath("$.description", is("This is an offer")))
                .andExpect(jsonPath("$.currency", is("GBP")))
                .andExpect(jsonPath("$.price", is(50.41)));
    }

}