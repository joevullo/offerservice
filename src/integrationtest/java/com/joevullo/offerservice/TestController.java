package com.joevullo.offerservice;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = {"/testcontroller"})
public class TestController {

    @RequestMapping(value = {"/testOk"}, method = RequestMethod.GET)
    public ResponseEntity<Object> getOK() {
        return ResponseEntity.ok("");
    }

    @RequestMapping(value = {"/testOfferNotFoundException"}, method = RequestMethod.GET)
    public ResponseEntity<Object> getOfferNotFoundException() throws OfferNotFoundException {
        throw new OfferNotFoundException("This is an offerNotFoundException");
    }

    @RequestMapping(value = {"/testIllegalArgumentException"}, method = RequestMethod.GET)
    public ResponseEntity<Object> getIllegalArgumentException() {
        throw new IllegalArgumentException("This is an IllegalArgumentException");
    }

}

