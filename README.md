# Offer Service

Author: Joseph Vullo 

Built using:

	* Spring Boot 
	* Spring MVC
	* Spring Data
	* Using H2 as a Data Source
	* Swagger
	* Maven 
	
You can run the application with:

```
mvn spring-boot:run
```
	
I've added swagger to the project so you can see the API made. 
	
```
http://localhost:8080/offerservice/swagger-ui.html
```
 